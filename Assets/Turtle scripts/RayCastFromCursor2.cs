﻿using UnityEngine;
using System.Collections;
using Vuforia;

public class RayCastFromCursor2 : MonoBehaviour
{
    //public GameObject cardboardMainObject;
    // Use this for initialization
    void Start()
    {
        CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
    }

    // Update is called once per frame
    void Update()
    {
#if !UNITY_WSA_10_0
        if (Cardboard.SDK.Triggered)
        {
#endif
#if UNITY_WSA_10_0
        if(Input.GetMouseButtonDown(0) || Input.touchCount > 0) {
#endif
            Ray ray;
                RaycastHit raycastHit;
                print("Mouse Down");
                if (Cardboard.SDK.VRModeEnabled)
                {
                    Physics.Raycast(transform.position, transform.forward, out raycastHit);
                }
                else
                {
                    ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    Physics.Raycast(ray, out raycastHit);
                }
            if (raycastHit.collider != null)
            {
                if (raycastHit.collider.tag == "TRex")
                {
                    TRex_Controller temp = raycastHit.collider.transform.parent.GetComponentInChildren<TRex_Controller>();
                    temp.roarAnimStart();

                }

#if !UNITY_WSA_10_0
            if (raycastHit.collider.tag == "VrToggle")
                    {
                        ARVRToggle();
                    }
#endif

        }
#if UNITY_WSA_10_0
        }
#endif
#if !UNITY_WSA_10_0
    }
#endif




        /*Vector3 fwd = transform.TransformDirection(Vector3.forward);
        Ray spriteCursorRay = new Ray(transform.position, transform.forward);
        RaycastHit SpriteCursorRaycastHit;
        Physics.Raycast(spriteCursorRay, out SpriteCursorRaycastHit);
        if (SpriteCursorRaycastHit.collider != null)
        {
            if (Input.GetMouseButtonDown(0)&&(SpriteCursorRaycastHit.collider.tag == "T-Rex_A" || SpriteCursorRaycastHit.collider.tag == "T-Rex_B")){
                //print("Hit T-Rex");
                SpriteCursorRaycastHit.collider.transform.GetComponent<TRex_Controller>().Roar();
            }
        }*/
    }
    public void ARVRToggle()
    {
        //cardboardMainObject.transform.GetComponent<Cardboard>().VRModeEnabled = !(cardboardMainObject.transform.GetComponent<Cardboard>().VRModeEnabled);
        Cardboard.SDK.VRModeEnabled = !(Cardboard.SDK.VRModeEnabled);
    }
}
