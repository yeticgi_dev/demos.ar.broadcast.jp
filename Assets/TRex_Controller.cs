﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using Vuforia;

public class TRex_Controller : MonoBehaviour {
    Animator anim;
    AudioSource roarSound;
    public Vector3 startPoint;
    public Vector3 animPoint;
	// Use this for initialization
	void Start () {
        anim = transform.GetComponent<Animator>();
        roarSound = transform.GetComponent<AudioSource>();
        CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
    }
	
	// Update is called once per frame
	void Update () {
	
	}
    public void roarAnimStart()
    {
        anim.SetBool("Roar", true);
        
    }
    public void TweenStart()
    {
        transform.DOLocalMove(animPoint, 2.5f).SetEase(Ease.InOutCubic);
    }
    public void playRoarSound()
    {
        roarSound.Play();
        
    }
    public void exitRoarAnim()
    {
        anim.SetBool("Roar", false);
        
    }
    public void TweenBack()
    {
        transform.DOLocalMove(startPoint, 3f).SetEase(Ease.InOutCubic);
    }

}
