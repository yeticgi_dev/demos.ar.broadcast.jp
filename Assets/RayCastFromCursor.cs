﻿using UnityEngine;
using System.Collections;
//using DG.Tweening;
using UnityEngine.UI;
using Vuforia;

public class RayCastFromCursor : MonoBehaviour
{
    bool enter = true;
    // Use this for initialization
    void Start()
    {
    }
    // Update is called once per frame
    void Update()
    {
        Vector3 fwd = transform.TransformDirection(Vector3.forward);
        Ray spriteCursorRay = new Ray(transform.position, transform.forward);
        RaycastHit SpriteCursorRaycastHit;
        Physics.Raycast(spriteCursorRay, out SpriteCursorRaycastHit);
        if (SpriteCursorRaycastHit.collider != null)
        {
            if (SpriteCursorRaycastHit.collider.tag == "TRex")
            {
                if (Input.GetMouseButton(0) && SpriteCursorRaycastHit.collider.tag == "TRex")
                {
                    SpriteCursorRaycastHit.collider.transform.GetComponent<TRex_Controller>().roarAnimStart();
                }
            }
        }
    }
}

